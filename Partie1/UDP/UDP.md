# Send data via UDP 

## Fonctionnement
        On peut utiliser le protocol UDP pour envoyer et recevoir des données. 
        Pour envoyer et recevoir des informations de control et des données sporadiques, on peut utiliser la même structure.
        Pour cela, il nous faut un serveur et un client. 
        Le serveur se met en écoute et recoit les données envoyés par le client.


### Exemple de code 

#### Envoyer et recevoir une donnée (chaine de caractère)

Il faut lancer le serveur puis l'émetteur ::

##### Extrait Code Serveur
Pour initialiser le socket :
```s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)``` 
```s.bind(("127.0.0.1", 9999))```
<\br>Pour recevoir des données :
```addr = s.recvfrom(1024)```
##### Extrait Code Emetteur
Pour initialiser le socket :
```s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)```
<\br>Pour envoyer des données :
```s.sendto(msgToSend, addrPort)```



#### Envoyer et recevoir une donnée (flux vidéo)

##### Extrait Code Serveur
Initialisation du socket :
```BUFF_SIZE = 65536```
```server_socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)```
```server_socket.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF,BUFF_SIZE)```
Récuperation d'un flux video et decodage :
```packet,_ = client_socket.recvfrom(BUFF_SIZE)```
```data = base64.b64decode(packet,' /')```
```....```
```cv2.imshow("RECEIVING VIDEO",frame)```


##### Extrait Code Emetteur
Initialisation du socket :
```BUFF_SIZE = 65536```
```server_socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)```
```server_socket.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF,BUFF_SIZE)```


Envoi d'un flux video et decodage :
