# ASI_Robia

ASI projet Robia

## Partie 1 :
### Comparatif UDP et TCP pour l'envoi de donnéees :
Pour envoyer des données de type classique, les deux protocoles se valent.

Pour l'envoi de flux video(en direct), le protocole UDP présente quelques avantages par rapport au protocol TCP. 

Le protocol UDP autorise la perte de paquet. Alors que le protocol TCP ne l'autorise pas. 
Le protocol TCP se met en attente quand un paquet se perd lors du transfert, les segments non acquittés par le/les clients sont mis en mémoire tampon. Les couches au-dessus devraient alors attendre toutes les retransmissions et ça causerait des délais intolérables.

Le protocol UDP lui, autorise la perte de paquet lors de l'envoi. Lors de l'envoi d'un flux video en direct, les paquets perdu ne pertubent pas le système, qui continue à envoyer et recevoir les données.



