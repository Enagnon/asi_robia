# ASI_Robia_Partie_1_TCP

## Envoie de Données.

1. Envoie de données Simples clientTCP.py/serveurTCP.py

Pour la première partie l'envoie de données simple tels que des string nous utilison une socket. Pour permettre la bonne transmition il faut indiqué le HOST ainsi que le PORT au client et au serveur.

###coté serveur 
Avnt de pouvoir communiqué il faut lancer le support de connexion ici une socket:

```
serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serveur.bind((ADRESSE, PORT))
```

Ici on crée la socket sur l'adresse et le port indiqué.

Pour lancée l'ecoute sur le serveur il faut lancer :

```serveur.listen(1)```

pour recevoir on utilise la méthode recv:

```donnees = client.recv(1024)```

Ici le 1024 représente la taile du message a lire sur le socket.

###coté client

De meme que pour le serveur il faut lancée la socket pour ouvrir le lien :

```
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
```

Pour que le client après avoir ouvert la connexion avec :

```client.connect((HOST, PORT))```

Nous pouvons maintenant envoyé le message au serveur grace à :

```n = client.send(message_Enc)```

ici message_Enc est notre message indiqué précedement dans le code.

###Lancé le code

pour executé lancée deux terminal dans le fichier contenant les deux fichiers python
Executé :

```
python3 client_TCP.py
python3 serveur_TCP.py
```

###Scénario 

Dans notre cas nous pouvons imaginé un client(moteur) qui doit etre démarée toute les n seconde 
pour pouvoir vérifié ce démarage le client envoie un start au serveur a chaque redémarage. Le serveur acquiesce la reception en renvoyant un start.


2. Envoie d'image ou plus généralement de fichié.


###Fonctionnement

Permet de transmettre via TCP un Fichier, dans notre cas une image.
Pour pouvoir utilisé ces codes il faut installer tqdm.

###Scénario 

Nous pouvons Imaginé un capteur qui envoie un fichier au serveur de manière ponctuel. Dans ce fichier nous avons les données que remonte le capteur 


###Lancé le code

pour executé lancée deux terminal dans le fichier contenant les deux fichiers python
Executé :

```
python3 img_client.py chemin-du-fichier
python3 img_serveur.py
```

3. Envoie dun flux video.


###Fonctionnement

Permet de transmettre via TCP un Flux Video. Ce flux video dans notre cas est récupérer sur la wabcam par le client et envoyé au serveur.


###Scénario 

Nous pouvons Imaginé un véhicule qui se deplace de manière autonome. Pour pouvoir analysé son environement le vehicule envoie un flux video a un serveur via TCP.


###Lancé le code

pour executé lancée deux terminal dans le fichier contenant les deux fichiers python
Executé :

```
python3 video_client.py
python3 video_serveur.py 
```
###Shéma de principe


```mermaid
graph TD;
  Camera-->Client;
  Client--TCP-->Serveur;
  Serveur-->Affichage;
```



