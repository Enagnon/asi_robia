import socket
ADRESSE = '127.0.0.1'
PORT = 6789

serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serveur.bind((ADRESSE, PORT))
serveur.listen(1)
client, adresseClient = serveur.accept()
print("Connexion de ", adresseClient)

donnees = client.recv(1024)
donnees_dec = donnees.decode()
if not donnees:
        print("Erreur de reception.")
else:
        print("Reception de:" + donnees_dec)

        reponse = donnees.upper()
        reponse_dec = reponse.decode()
        print("Envoi de :" + reponse_dec)
        n = client.send(reponse)
        if (n != len(reponse)):
                print("Erreur envoi.")
        else:
                print("Envoi ok.")

print("Fermeture de la connexion avec le client.")
client.close()
print("Arret du serveur.")
serveur.close()
