import socket

HOST = '127.0.0.1'
PORT = 6789

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
print("Connexion vers " + HOST + ":" + str(PORT) + " reussie.")

message = 'Start'
print("Envoi de :" + message)
message_Enc = message.encode()
n = client.send(message_Enc)
if (n != len(message_Enc)):
        print("Erreur envoi.")
else:
        print("Envoi ok.")

print("Reception...")
donnees = client.recv(1024)
print("Recu :", donnees)

print("Deconnexion.")
client.close()
