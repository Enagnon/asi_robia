# ASI_Robia_Partie_2_Flask

## Envoie de Données.

1. Envoie d'une image via flask 

### Image

Via Flask nous avons implémenté une une fonctionalité qui pêrmet l'envoie d'une image. L'image indiqué en entrant le chemin de l'image:

``` 
@app.route('/get_image')
def get_image():
    filename = 'image.jpg'
    return send_file(filename, mimetype='image/jpg')

```

Nous pouvons voir dans le code que le point d'entré qui permet cette fonctionnalité est /get_image.

2. Récuperation de données via flask


### Json

Lr programme suivant permet de lancer une requete http get depuis un programme python qui recupère un fichier JSON depuis le serveur flask

```
import requests

URL = 'http://localhost:8888/get/json'
r =requests.get(url=URL,)
data = r.json()
print(data)

```

## Comparaison.

### flask vs bottle vs Django vs Web2py

#### bottle

```
Bottle est un micro framework web rapide pour Python. Il n'a aucune dépendance en dehors de la bibliothèque standard Python et est si léger que le module correspondant est un fichier unique. Il gère tout ce dont vous avez besoin pour créer de petits sites Web ou applications. Il est également asynchrone, ce qui vous permet de maintenir facilement les données de votre application à jour en permanence. Une autre fonctionnalité intéressante est qu'il est livré avec un serveur de développement HTTP intégré.

C'est un excellent choix si vous construisez quelque chose de petit ou si vous voulez rapidement prototyper une idée. Parce qu'il est simple à utiliser, il est également idéal pour les nouveaux développeurs. Vous pouvez facilement comprendre comment utiliser Bottle pour tout projet que vous créez et préparer rapidement votre application pour la production.

Un inconvénient de Bottle est qu'il a moins de documentation et de support que Flask ou Django. Si vous souhaitez créer une grande application Web complexe, vous trouverez peut-être que vos efforts seront mieux soutenus en utilisant l'un des autres outils. Il existe également un certain nombre de choses que Bottle n'a pas intégrées, telles qu'un panneau d'administration, un framework ORM, la prise en charge de NoSQL, la prise en charge de REST, la sécurité, les formulaires Web ou l'authentification. Cependant, il existe un certain nombre de plugins et de bibliothèques que vous pouvez utiliser pour ajouter ces éléments à Bottle si vous le souhaitez.

Le point d'achoppement reviendra probablement à tout ce qui n'a pas beaucoup de documentation avec laquelle travailler.
```

#### flask

```
De nombreux développeurs choisissent Flask plutôt que Bottle parce qu'ils estiment qu'il offre tout ce que Bottle fait et plus encore. Je dirais que ce qu'il a sur Bottle, c'est la popularité, ce qui conduit à plus de documentation, d'extensions et de plugins conçus pour être utilisés avec Flask. Si vous êtes un développeur plus récent qui essaie de faire quelque chose de plus complexe, Flask pourrait être un meilleur choix que Bottle, car il est probable que quelqu'un ait déjà documenté un moyen de faire tout ce sur quoi vous travaillez. C'est un tirage au sort. Avec Bottle, vous pouvez probablement lire tout le code du module. Avec Flask, vous obtenez des tonnes de contenu pour soutenir votre projet.

Le flacon est génial avec les extensions. En les utilisant, vous pouvez ajouter un panneau d'administration, prendre en charge REST et prendre en charge les formulaires Web. Il offre une sécurité intégrée, mais elle est minime. Vous pouvez augmenter la sécurité de Flask avec des outils comme Flask-Security, mais vous devrez rester au courant des mises à jour pour vous assurer que les vulnérabilités sont corrigées au fur et à mesure qu'elles sont découvertes.

Icône de validation par la communauté
```
#### Django

```
Django est assez différent de Bottle ou de Flask. Si vous débutez et que vous souhaitez simplement créer un exemple de code ou une petite application, je vous recommande immédiatement de choisir entre Bottle et Flask pour votre projet. Django est incroyablement puissant, gère toutes sortes de choses pour vous, ce qui vous facilite la vie, mais il a une courbe d'apprentissage abrupte.

Django est un framework Web qui permet de développer rapidement et facilement des applications Web… tant que vous avez le temps d'apprendre le framework en premier. L'une des meilleures caractéristiques est qu'il est sécurisé. Si vous ne connaissez pas grand-chose à la sécurité des applications, Django a été conçu pour vous aider à protéger votre site Web. Il vous offre un moyen sécurisé de gérer les comptes et les mots de passe et vous évite de commettre des erreurs comme mettre des informations de session dans les cookies. Il permet une protection contre les vulnérabilités telles que l'injection SQL, les scripts intersites, la contrefaçon de requêtes intersites et le détournement de clics.

Bottle and Flask vous permet de créer des sites qui incluent ces éléments, mais ils ne sont pas gérés pour vous. Si vous êtes un développeur expérimenté qui sait comment éviter ces erreurs de sécurité, alors Bottle ou Flask peuvent toujours être d'excellents choix. Sinon, apprendre Django peut vous éviter bien des maux de tête en matière de sécurité.

En ce qui concerne la documentation, Flask bat probablement encore Django. Cependant, si vous êtes prêt à consacrer du temps à apprendre Django, cela en vaut la peine. Il a beaucoup de choses que vous voulez faire disponibles hors de la boîte. La plupart des choses que vous voudriez faire (par exemple, gérer les sessions, l'authentification, la gestion des utilisateurs, la gestion de contenu, etc.) sont disponibles dans Django. Et bien qu'il y ait moins de documentation, il y a encore beaucoup de choix.
```
#### Web2py

```
Web2py est un framework full-stack évolutif et open source pour Python, livré avec son propre IDE basé sur le Web. Web2py n'a aucune exigence d'installation et de configuration, possède des fonctionnalités de lisibilité pour plusieurs protocoles et peut fonctionner sur Windows, Mac, Linux/Unix, Google App Engine, Amazon EC2 et tout hébergement Web prenant en charge Python 2.5+. Web2py est également rétrocompatible. Ses principales fonctionnalités incluent le contrôle d'accès basé sur les rôles (RBAC), la couche d'abstraction de base de données (DAL) et la prise en charge de l'internationalisation. Il est à noter que la conception de web2py a été inspirée par Django.

```

