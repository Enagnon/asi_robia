#!flask/bin/python
from flask import Flask, jsonify
from flask import send_file

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/get_image')
def get_image():
    filename = 'image.jpg'
    return send_file(filename, mimetype='image/jpg')


@app.route('/get/json', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

@app.route('/post', methods=['POST'])
def post_data():
    dt = request.args.get('data')
    return '''<h1>La valeur recue est : {}</h1>'''.format(dt)

if __name__ == '__main__':
    app.run(host="127.0.0.1",port=8888,debug=True) 
