#!flask/bin/python
from flask import Flask, render_template, Response
import cv2
#from camera import Camera


app = Flask(__name__)

camera = cv2.VideoCapture(0)
success,image = camera.read()
#cam = Camera()

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(image,
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/')
def index():
    return "Hello, World!"


if __name__ == '__main__':
    app.run(host="127.0.0.1",port=8888,debug=True)
