# Deployer une application flask sur docker

## docker-flask.

Le premier dossier contient les fichier necessaire au déploiment de flask sur docker.
Pour utilisé cette application rendez vous dans le /docker-flask et lancé les commandes suivantes:

Pour Build l'image faite
```
$ sudo docker build -t docker-flask
```
Pour lancer l'image dans un container 
```
$ sudo docker run -d -p 8080:8080 docker-flask
```

Vous pouvez maintenant accedées a l'application via ladresse ```localhost:8080```


## docker-flask-with-nginx.

Le second dossier contient un une autre application python simple mais qui utilise Nginx. Pour lancée l'applicaion dans /docker-flask-with-nginx lancée les commandes:

Pour Build l'image faite
```
$ sudo docker build -t docker-flask-with-nginx
```
Pour lancer l'image dans un container 
```
$ sudo docker run -d -p 8080:8080 docker-flask-with-nginx
```

Vous pouvez maintenant accedées a l'application via ladresse ```localhost:8080```


### docker-flask-with-nginx-full.

Ce dossier correspond au déploiement d'une application web avec docker, nginx et flask. 

