#Partie3
## Déploiement d'une application web Python sur un serveur avec Nginx et uWSGI

### Généralités 
WSGI : Web Server Gate Interface

Contrairement au Php, où il suffit de poser ses fichiers à côté des fichiers HTML, directement interprétés à la volée par le serveur web, sur une application Python, on distingue clairement les différents services et protocoles qui entrent en jeux.

Navigateur Web → HTTP → Serveur WSGI → WSGI → Application
Application → WSGI → Serveur WSGI → HTTP → Navigateur Web
1. 
```mermaid
graph TD;
  Navigateur_Web--HTTP-->Serveur_WSGI--WSGI-->Application;
  
```
2.
```mermaid
graph TD;
  Application--WSGI-->Serveur_WSGI--HTTP-->Navigateur_Web;
  
```
D'un coté, on a notre application Python qui va recevoir une requête HTTP par le protocole WSGI sous forme d'objet Python. Elle va la traiter, générer une réponse, une page web par exemple, et envoyer cette réponse sous forme d'objet Python par le protocole WSGI.

De l'autre coté, vous avez le serveur (d'application), qui doit donc être capable de recevoir des requêtes HTTP envoyées par les navigateurs web, de les transformer en objets Python, de les transmettre par le protocole WSGI, d'attendre les réponses sous forme d'objet Python et de les renvoyer au client en HTTP par le réseau.

Les serveurs WSGI sont souvent livrés avec les framework web à des fins de test, typiquement Werkzeug (avec Flask), CherryPy, Bottle ou encore Django. La plupart d'entre eux ne sont pas recommandés en production. Pour la production, il existe des serveurs en daemons, tels que Apache (avec mod_wsgi) et Gunicorn.

Nginx est apprécié pour son efficacité et sa simplicité de configuration. Malheureusement, il ne gère pas WSGI, et ne peut donc pas communiquer directement avec notre application. Nous allons utiliser uWSGI qui est à la fois rapide et simple à configurer.

Nginx est un excellent proxy qui, en plus de transférer les requêtes à uWSGI, va pouvoir délivrer directement les fichiers statiques (images, css, js…), faire office de cache, va pouvoir communiquer en HTTPS, et va même compresser les réponses avec gzip. De plus, il sait très bien discuter en HTTP avec les navigateurs.

Nginx va donc se charger de parser les requêtes HTTP pour les transférer à uWSGI.
